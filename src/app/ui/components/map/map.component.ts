import { Component, OnInit } from '@angular/core';
import * as L from "leaflet";
import { BmCapteursVelosService } from 'src/app/services/bm-capteurs-velos.service';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  options = {
    layers: [
      L.tileLayer(
        'https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png',
        {
          maxZoom: 18,
          attribution: '<a href="https://github.com/cyclosm/cyclosm-cartocss-style/releases" title="CyclOSM - Open Bicycle render">CyclOSM</a> | Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        })
    ],
    zoom: 12,
    center: L.latLng(44.8414,-0.5864),
    zoomControl: false
  };

  constructor(private bmCapteurService: BmCapteursVelosService) { }


  ngOnInit(): void {

  }

  onMapReady(leafletMap: L.Map) {
    this.bmCapteurService.getCaptors().subscribe((data) => {
      L.geoJSON(
        data as GeoJSON.GeoJsonObject,
        {
          onEachFeature: function(feature, layer){
            layer.bindTooltip(feature.properties.name)
          },
          pointToLayer: function(feature, latlng) {
            return new L.CircleMarker(latlng, {
              radius: 2,
              color: 'red'
            });
          },
        }
        ).addTo(leafletMap)
    });
  }

}
