import { DatePipe,registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import localeFr from '@angular/common/locales/fr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Router } from '@angular/router';
import { UiModule } from './ui/ui.module';

registerLocaleData(localeFr, 'fr');
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    UiModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR'},
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
      // Diagnostic only: inspect router configuration
      constructor(router: Router) {
        // Use a custom replacer to display function names in the route configs
        const replacer = (key: string, value: any) => (typeof value === 'function') ? value.name : value;
        ​
        console.log('Routes: ', JSON.stringify(router.config, replacer, 2));

        registerLocaleData(localeFr, 'fr-FR');
  }
}
