import { TestBed } from '@angular/core/testing';

import { BmCapteursVelosService } from './bm-capteurs-velos.service';

describe('BmCapteursVelosService', () => {
  let service: BmCapteursVelosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BmCapteursVelosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
