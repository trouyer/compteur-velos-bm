import { DatePipe } from '@angular/common';
import { Inject, LOCALE_ID } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { toArray } from 'rxjs/operators';
import { CaptorData } from 'src/app/models/data';
import { BmCapteursVelosService } from 'src/app/services/bm-capteurs-velos.service';

@Component({
  selector: 'app-page-captor-today',
  templateUrl: './page-captor-today.component.html',
  styleUrls: ['./page-captor-today.component.scss']
})
export class PageCaptorTodayComponent implements OnInit {

  captorDatas: CaptorData[][]= []

  currentDate = this.datepipe.transform(new Date(), 'dd MMMM yyyy')


  constructor(
    private bmCapteurService: BmCapteursVelosService,
    private datepipe: DatePipe) { }



  ngOnInit(): void {
    //this.drawBars(this.data);
    this.getData();
  }

  private getData() {
    this.bmCapteurService.getCurrentDayData().pipe(
      toArray()
    ).subscribe((d) => {
      const captorNames = [...new Set(d.map(x => x.name).sort((a,b) => a.localeCompare(b)))];

      captorNames.forEach(name => {
        const a = d.filter(x => x.name == name)
        this.captorDatas.push(
          d.filter(x => x.name == name)
        )}
      );
    });
  }
}
