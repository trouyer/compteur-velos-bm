import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageCaptorTodayComponent } from './pages/page-captor-today/page-captor-today.component';
import { PageCaptorDetailsComponent } from './pages/page-captor-details/page-captor-detail.component';

const routes: Routes = [
  { path: '', component: PageCaptorTodayComponent},
  { path: 'details/:id' , component: PageCaptorDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaptorsRoutingModule { }
