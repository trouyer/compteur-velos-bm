import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import { CaptorDetailsI } from 'src/app/models/captor-details-i';
import { BmCapteursVelosService } from 'src/app/services/bm-capteurs-velos.service';

@Component({
  selector: 'app-page-captor-detail',
  templateUrl: './page-captor-detail.component.html',
  styleUrls: ['./page-captor-detail.component.scss']
})
export class PageCaptorDetailsComponent implements OnInit {

  @ViewChild(BaseChartDirective) chart!: BaseChartDirective;

  captorName: string = ""
  captorGid!: number


  constructor(
    private route: ActivatedRoute,
    private bmCapteurService: BmCapteursVelosService,
    private datepipe: DatePipe) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (params) => {
        const id = params.get('id')

        if(id)
        {
          this.captorGid = parseInt(id)
          this.captorName = this.bmCapteurService.getName(this.captorGid)

          this.GetDataAndDrawGraph(this.captorGid )
        }
      }
    )
  }

  private GetDataAndDrawGraph(gid: number){
    this.bmCapteurService.getCaptorDetailsByDay(gid)
    .subscribe((d) => {
      this.drawLines(d)

    });
  }

  private drawLines(data: CaptorDetailsI) {

    this.lineChartLabels = data.time.map(x => {
      const date = this.datepipe.transform(x, 'dd/MM/yyyy')

      return date ? date.toString() : ""
    });


    this.fillDaysOff(data.time);


    this.lineChartDataSets.push(
      {
        data: data.count,
        label: this.captorName,
        borderWidth: 1
      }
    );

    this.chart.ngOnChanges({});
  }

  private fillDaysOff(date: Date[]) {

    let annotations: pluginAnnotations.AnnotationOptions[] = []

    let i = 0
    while(i < date.length) {
      if ( new Date(date[i]).getDay() == 6){
        annotations.push(
          this.getBoxAnnotation(this.lineChartLabels[i].toString(), this.lineChartLabels[i+1].toString())
        )
        i += 7;
      }
      else {
        i++
      }
    }

    if(this.chart.options.annotation)
      this.chart.options.annotation.annotations = annotations
  }

  private getBoxAnnotation(label1: string, label2: string): any{
    return {
      drawTime: "beforeDatasetsDraw",
      type: "box",
      xScaleID: "x-axis-0",
      xMin: label1,
      xMax: label2,
      backgroundColor: "Gainsboro",
      borderWidth: 0
    }
  }

  lineChartDataSets: ChartDataSets[] = []

  // Labels
  lineChartLabels: Label[] = [];


  lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    },
    elements: {
      line: {
        tension: 0
      }
    },
    annotation: {
      annotations:[]
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#42cbf5',
    },
  ];

  lineChartLegend = false;
  lineChartPlugins = [pluginAnnotations];
  lineChartType = 'line' as ChartType;


}
