from datetime import date,datetime, timedelta
import requests
import json
import os
import sys
from functools import reduce
from pathlib import Path

Path("./data").mkdir(parents=True, exist_ok=True)

requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += 'HIGH:!DH:!aNULL'


if not 'API_KEY' in os.environ:
    print("Error: set API_KEY environment variable. Get an API key from https://data.bordeaux-metropole.fr/opendata/key")
    sys.exit(1)


apiKey = os.environ.get('API_KEY')
baseEndpoint = f"https://data.bordeaux-metropole.fr/geojson/aggregate/pc_captv_p?key={ apiKey }"
rangeStart = "&rangeStart="
startDate = date(2020, 12, 1)
endDate = date.today() - timedelta(days=1)

def getCaptorDetails(rangeStep):
    url = f"{baseEndpoint}&rangeStart={startDate}&rangeEnd={endDate}&rangeStep={rangeStep}"

    try:
        print(f"get data from {url}...")
        r = requests.get(url)

        if r.status_code != 200:
            print(f"Requests error: { r }")
            sys.exit(1)
        featuresCollection = json.loads(r.text)
        features = [feature for feature in featuresCollection['features'] if feature["properties"]["type"] == 'BOUCLE' ]
        mapCaptor(features, rangeStep)
    except:
        print("Unexpected error:", sys.exc_info()[0])

def mapCaptor(features, rangeStep):


    multiplicator = 12
    if rangeStep == "day":
        multiplicator = multiplicator * 24

    gids = set([ feature["properties"]["gid"] for feature in features ])

    for gid in gids:
        captorDetail = {
          "gid": gid,
          "time": [ feature['properties']['time'] for feature in features if feature["properties"]["gid"] == gid],
          "count": [ round(feature['properties']['comptage_5m']* multiplicator)  for feature in features if feature["properties"]["gid"] == gid]
        }


        with open(f"data/{gid}_byDay.json", "w") as output:
            output.write(json.dumps(captorDetail))

getCaptorDetails("day")
