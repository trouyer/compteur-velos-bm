import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./captors/captors.module').then(
      m => m.CaptorsModule
    )
  },
  {
    path: 'captors',
    loadChildren: () => import('./captors/captors.module').then(
      m => m.CaptorsModule
    )
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
