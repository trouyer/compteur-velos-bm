import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaptorsRoutingModule } from './captors-routing.module';
import { PageCaptorTodayComponent } from './pages/page-captor-today/page-captor-today.component';
import { PageCaptorDetailsComponent } from './pages/page-captor-details/page-captor-detail.component';
import { TimelineComponent } from './component/timeline/timeline.component';
import { ChartsModule } from 'ng2-charts';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [PageCaptorTodayComponent, PageCaptorDetailsComponent, TimelineComponent],
  imports: [
    CommonModule,
    RouterModule,
    CaptorsRoutingModule,
    ChartsModule
  ]
})
export class CaptorsModule { }
