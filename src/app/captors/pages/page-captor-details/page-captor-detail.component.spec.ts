import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCaptorDetailsComponent } from './page-captor-detail.component';

describe('PageCaptorHistoryComponent', () => {
  let component: PageCaptorDetailsComponent;
  let fixture: ComponentFixture<PageCaptorDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageCaptorDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCaptorDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
