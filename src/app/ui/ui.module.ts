import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiComponent } from './containers/ui/ui.component';
import { HeaderComponent } from './components/header/header.component';
import { MapComponent } from './components/map/map.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [UiComponent, HeaderComponent, MapComponent],
  imports: [
    RouterModule,
    CommonModule,
    LeafletModule
  ],
  exports: [
    UiComponent
  ]
})
export class UiModule { }
