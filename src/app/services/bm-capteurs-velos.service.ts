import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { filter, map, mergeMap } from 'rxjs/operators';
import { CaptorData, Data } from '../models/data';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common'
import { CaptorDetailsI } from '../models/captor-details-i';

@Injectable({
  providedIn: 'root'
})
export class BmCapteursVelosService {

  private endpoint = "https://data.bordeaux-metropole.fr/geojson/"

  constructor(private http: HttpClient, private datepipe: DatePipe) { }

  getCurrentDayData() {
    const today  = new Date();
    today.setHours(today.getHours() - 1);

    const request = `${ this.endpoint }aggregate/pc_captv_p?key=${ environment.API_KEY }&rangeStart=${ this.datepipe.transform(today, 'yyyy-MM-dd') }&rangeStep=hour`

    return this.http.get<GeoJSON.FeatureCollection>(request).pipe(
        mergeMap((x: GeoJSON.FeatureCollection) => x.features),
        filter(f => f.properties != null && f.properties.gid != undefined && f.properties.type == "BOUCLE"),
        map((f: GeoJSON.Feature) => {
          {
            let d = new CaptorData()
            d.gid =  f.properties?.gid
            d.name = this.getName(f.properties?.gid)
            d.time = f.properties?.time
            d.counter =  Math.round(f.properties?.comptage_5m * 12)
            return d;
          }
        })
      );
  }

  getName(gid: number){
    const captorNames: {[key: number]: string} = {
      2447: "Pont de pierre vers Bir-Hakeim",
      2451: "Pont de pierre vers Stalingrad",
      2783: "Général de Gaulle vers nord",
      2784: "Général de Gaulle vers sud",
      4281: "Thouars vers nord",
      4282: "Thouars vers sud",
      2436: "François Mittérand vers Bordeaux",
      2437: "François Mittérand vers Aéroport",
      2441: "Barrière St Augustin vers nord",
      2443: "Barrière St Augustin vers sud",
      2444: "Barrière St Augustin vers ouest",
      2445: "Barrière St Augustin vers est",
      2440: "Barrière Judaïque vers ouest",
      2442: "Barrière Judaïque vers est",
      2452: "Maréchal Juin vers ouest",
      2453: "Maréchal Juin vers est",
      2777: "Georges Clémenceau vers Tourny",
      2778: "Georges Clémenceau vers Gambetta",
      2448: "Cours de Verdun vers Doumer",
      2449: "Cours de Verdun vers Tourny",
      2779: "Pont St-Jean sortant",
      2780: "Pont St-Jean entrant",
      2781: "Quai de la Souys vers pont Simone Veil",
      2782: "Quai de la Souys vers pont St Jean",
      2787: "Avenue Jean Zay vers ouest",
      2788: "Avenue Jean Zay vers est",
      2785: "Avenue de Paris vers sud",
      2786: "Avenue de Paris vers nord",
      2446: "Avenue des Griffons vers sud",
      2450: "Avenue des Griffons vers nord",
      4311: "Avenue de Magudas vers ouest",
      4312: "Avenue de Magudas vers est"
    }

    if(captorNames[gid])  {
     // console.warn("Captor without name!", [{"gid": gid}])
      return captorNames[gid];
    }

    return gid.toString();
  }

  getCaptors(){
    return this.http.get<GeoJSON.FeatureCollection>('./assets/data/captors.geojson')
  }

  getHistoryData() {
    return this.http.get<Data>('./assets/data/dataHistory.json').pipe(
        map((d) => {
          d.name = this.getName(d.gid)
          d.timestamp = Date.parse(d.time)
          return d;
        }
      )
    )
  }

  getCaptor(gid: number) {
    return this.http.get<Data>('./assets/data/dataHistory.json').pipe(
        filter(x => x.gid == gid),
        map((d) => {
          let c = new CaptorData()
          c.gid =  d.gid
          c.name = this.getName(d.gid)
          c.time = d.time
          c.counter = d.counter
          return c;
        }

      )
    )
  }
  getCaptorDetailsByDay(gid: number) {
    return this.http.get<CaptorDetailsI>(`./assets/data/${gid}_byDay.json`)
  }
}
