import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCaptorTodayComponent } from './page-captor-today.component';

describe('PageCaptorTodayComponent', () => {
  let component: PageCaptorTodayComponent;
  let fixture: ComponentFixture<PageCaptorTodayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageCaptorTodayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCaptorTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
