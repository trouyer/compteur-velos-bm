import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChartDataSets, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { CaptorData } from 'src/app/models/data';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  @Input()
  data: CaptorData[] = [];

  captorName: string = ""
  captorGid!: number
  constructor(
    private router: Router,
    private datepipe: DatePipe) { }

  ngOnInit(): void {
    this.drawLines(this.data)
  }

  private drawLines(data: CaptorData[]) {
    this.lineChartLabels = [...new Set(data.map(x => {
      let date = Date.parse(x.time)

      let hour =  this.datepipe.transform(date, 'h:mm a zzzz')
      return hour ? hour : ""
    }))]


    this.lineChartDataSets.push(
      {
        data: data.map(y => y.counter),
        //label: data[0].name.toString()
        borderWidth: 1
      }
    );

    this.captorName = data[0].name
    this.captorGid = data[0].gid
  }

  lineChartDataSets: ChartDataSets[] = []

  // Labels
  lineChartLabels: Label[] = [];


  lineChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          precision:0
        }
      }]
    },
    elements: {
      line: {
        tension: 0
      }
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#42cbf5'
    },
  ];

  lineChartLegend = false;
  lineChartPlugins = [];
  lineChartType = 'line' as ChartType;

  gotoDetailCaptor(gid: number){
    this.router.navigate(['/captors', 'details', gid]);
  }
}
